package com.nuc.masterplan;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@ManagedBean(name = "helloWorld", eager = true)
@ViewScoped
public class Hello {

    Calendar calendar = Calendar.getInstance();

    public static final ThreadLocal<SimpleDateFormat> DATE_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat("dd MMM yyyy"));

    public String getMessage() {
        return "Welcome to MasterPlan. WIP 0.1";
    }

    public String getVersion() {
        return "0.1";
    }

    public String getDate() {
        return DATE_FORMAT.get().format(calendar.getTime());
    }

    public void previous() {
        calendar.add(Calendar.DAY_OF_YEAR, -1);
    }

    public void next() {
        calendar.add(Calendar.DAY_OF_WEEK, 1);
    }
}
